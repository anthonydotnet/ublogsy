﻿using System;

namespace uBlogsy.BusinessLogic.EventHandlers
{
    using Umbraco.Core.Events;
    using Umbraco.Core.Models;
    using Umbraco.Core.Services;

    using Umbraco.Core;

    public class UmbracoNodeEventsForPosts : IApplicationEventHandler
    {
        public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }



        public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {

        }


        /// <summary>
        /// Wire up events.
        /// </summary>
        /// <param name="umbracoApplication"></param>
        /// <param name="applicationContext"></param>
        public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            if (applicationContext.IsConfigured && applicationContext.DatabaseContext.IsDatabaseConfigured)
            {
                ContentService.Created += Content_New;
                ContentService.Saving += Content_Saving;
                ContentService.Saved += Content_Saved;
            }
        }

        /// <summary>
        /// Set the blog post properties on document create
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void Content_New(IContentService sender, NewEventArgs<IContent> e)
        {
            // Set current date.
            if (e.Entity.HasProperty("uBlogsyPostDate"))
            {
                e.Entity.SetValue("uBlogsyPostDate", DateTime.Now);
            }
        }


        /// <summary>
        /// Set tag/category default property values on document saving
        /// We can't do these properties on create as the source values will not exist
        /// Its better to use Saving here as its not neccesary to call .Save after setting the values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void Content_Saving(IContentService sender, SaveEventArgs<IContent> e)
        {
            foreach (var entity in e.SavedEntities)
            {
                if (entity.ContentType.Alias == "uBlogsyLabel" && string.IsNullOrWhiteSpace(entity.GetValue<string>("uBlogsyLabelName")))
                {
                    entity.SetValue("uBlogsyLabelName", entity.Name);
                }

                if (entity.ContentType.Alias == "uTagsyTag" && string.IsNullOrWhiteSpace(entity.GetValue<string>("uTagsyTagName")))
                {
                    entity.SetValue("uTagsyTagName", entity.Name);
                }
            }
        }

        /// <summary>
        /// Ensures that node name is the same as post title.
        /// </summary>
        private static void Content_Saved(IContentService sender, SaveEventArgs<IContent> e)
        {
            foreach (var entity in e.SavedEntities)
            {
                if (entity.ContentType.Alias == "uBlogsyPost" && entity.ParentId != -20)
                {
                    PostService.Instance.EnsureCorrectPostTitle(entity);
                    PostService.Instance.EnsureCorrectPostNodeName(entity);
                    PostService.Instance.EnsureNavigationTitle(entity);
                }
            }
        }

    }
}